<?php

use App\Http\Controllers\LeagueController;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\SeasonController;
use App\Http\Controllers\TeamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')
    ->group(function () {
        Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout')
            ->name('logout.api');

        Route::get('/user','App\Http\Controllers\UserController@userdata')
            ->name('user.api');

    });
Route::middleware(['auth:sanctum', 'permitted'])
    ->group(function () {
        Route::post('/match','App\Http\Controllers\MatchController@create');
        Route::put('/match/{match}','App\Http\Controllers\MatchController@update');
        Route::delete('/match/{match}','App\Http\Controllers\MatchController@destroy');
        Route::apiResource('players', PlayerController::class)
            ->only(['store', 'update', 'destroy']);
    });

Route::post('/register','App\Http\Controllers\Auth\RegisterController@register')
    ->name('register.api');
Route::post('/login','App\Http\Controllers\Auth\LoginController@login')
    ->name('login.api');

Route::get('/match/lastMatches', [MatchController::class, 'lastMatches']);
Route::get('/match/nextMatches', [MatchController::class, 'nextMatches']);

Route::get('/leagues', [LeagueController::class, 'index']);

Route::prefix('seasons/{season}')->group(function () {
    Route::get('/', [SeasonController::class, 'show'])->name('show');
    Route::get('teams', [SeasonController::class, 'teams'])->name('teams');
    Route::get('teams/{team}', [SeasonController::class, 'team'])->name('teams.team');
    Route::prefix('stages')->group(function () {
        Route::get('/', [SeasonController::class, 'stages'])->name('stages');
        Route::post('/', [SeasonController::class, 'store'])
            ->name('create-stage');
    });
    Route::get('referees', [SeasonController::class, 'referees'])->name('referees');
    Route::get('table', [SeasonController::class, 'table']);
});

Route::apiResource('players', PlayerController::class)
    ->except(['store', 'update', 'destroy']);

Route::prefix('/players')->group(function () {
    Route::get('ratings/{season}', [PlayerController::class, 'ratings']);
    Route::get('misses/{season}', [PlayerController::class, 'misses']);
});

Route::post('teams/{team}/members', [TeamController::class, 'members'])->name('teams.members');

Route::prefix('match/{match}')->group(function () {
    Route::get('', [MatchController::class, 'index']);
    Route::get('players', [MatchController::class, 'players']);
    Route::get('data', [MatchController::class, 'data']);
});
