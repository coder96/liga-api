<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class CheckPermissions
{
    public function handle($request, Closure $next)
    {
        $controller = class_basename(Route::current()->controller);
        $method = Route::current()->getActionMethod();

        $user = $request->user();
        if ($user &&
            ($user->hasPermissions($controller, $method) || $user->role === 'root')){
            return $next($request);
        }

        return response()->json(['error' => 'Access denied'], 403);
    }
}
