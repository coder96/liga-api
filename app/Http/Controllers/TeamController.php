<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamMemberRequest;
use App\Models\Player;
use App\Models\Team;

class TeamController extends Controller
{
    public function members(Team $team, TeamMemberRequest $teamMemberRequest): int
    {
        $params = $teamMemberRequest->validated();
        if (!$team->players($params['season_id'])->where('PlayerId', '=', $params['player_id'])->exists()) {
            $team->players()->attach($params['player_id'], [
                'SeasonID' => $params['season_id'],
                'TeamID' => $team->id
            ]);
            $player = Player::find($params['player_id']);
            $player->leftFromTeam($player->TeamID, (int)$params['season_id']);
            $player->TeamID = $team->id;
            $player->save();
        }
        return 1;
    }
}
