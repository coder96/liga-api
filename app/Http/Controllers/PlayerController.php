<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerRequest;
use App\Models\Card;
use App\Models\Player;
use App\Models\PlayerMember;
use App\Models\Season;

class PlayerController extends Controller
{
    public function index()
    {
        return Player::all();
    }

    public function update(Player $player, PlayerRequest $request)
    {
        return $player->update($request->validated());
    }

    public function store(PlayerRequest $request)
    {
        $player = Player::create($request->validated());

        PlayerMember::create([
            'PlayerID' => $player->id,
            'TeamID' => $player->TeamID,
            'SeasonID' => $request->SeasonID,
        ]);

        return $player;
    }

    public function ratings(Season $season)
    {
        return [
            'bombardiers' => $season->bombardiers(),
            'violators' => $season->violators(),
        ];
    }

    public function misses(Season $season)
    {
        return [
            'yellow' => Card::playerMisses($season->id, yellowCardCount: $season->yellow_card_count),
            'red' => Card::playerMisses($season->id, 1),
            'yellow_card_count' => $season->yellow_card_count,
        ];
    }

}
