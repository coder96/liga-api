<?php

namespace App\Http\Controllers;

use App\Http\Requests\StagePostRequest;
use App\Http\Resources\SeasonResource;
use App\Http\Resources\StageResource;
use App\Models\Referee;
use App\Models\Season;
use App\Models\Team;
use App\Services\StageService;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Models\Season $season
     * @param \App\Http\Requests\StagePostRequest $request
     * @param \App\Services\StageService $service
     * @return \Illuminate\Http\Response
     */
    public function store(Season $season, StagePostRequest $request, StageService $service)
    {
        return $service->create([
            'SeasonID' => $season->id,
            'Name' => $request->name,
            'type' => $request->type,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Season  $season
     * @return \App\Http\Resources\SeasonResource
     */
    public function show(Season $season): SeasonResource
    {
        return new SeasonResource($season);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function edit(Season $season)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Season $season)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function destroy(Season $season)
    {
        //
    }

    public function teams(Season $season)
    {
        return $season->teams()->get();
    }

    public function team(Season $season, Team $team)
    {
        $team->players = $team->players($season->id)->get();
        foreach ($team->players as $player) {
            $player->FullName = $player->getFullName();
        }
        return $team;
    }

    public function stages(Season $season)
    {
        return StageResource::collection(
            $season->stages()
                ->orderBy('id')
                ->get()
        );
    }

    public function referees()
    {
        return Referee::all();
    }

    public function table(int $seasonID)
    {
        $season = Season::find($seasonID);
        return $season->table($seasonID);
    }
}
