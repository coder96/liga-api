<?php

namespace App\Http\Controllers;

use App\Http\Requests\MatchRequest;
use App\Models\Card;
use App\Models\Goal;
use App\Models\Game;
use App\Repositories\MatchRepository;
use App\Services\MatchMessageService;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function index(int $id)
    {
        return Game::with([
            'guestTeam.players',
            'homeTeam.players',
            'goals',
            'cards',
            'stage'
        ])->find($id);
    }

    public function lastMatches()
    {
        return (new Game())->lastMatches();
    }

    public function nextMatches()
    {
        return Game::orderBy('Date', 'ASC')
            ->where('IsCompleted', '=', 0)
            ->where('Date', '>=', date('Y-m-d'))
            ->take(6)
            ->with(['homeTeam', 'guestTeam', 'stage'])
            ->get();
    }

    public function create(MatchRequest $matchRequest, MatchRepository $repository)
    {
        if (
            $repository->existsInStage($matchRequest->StageID, $matchRequest->HomeTeamID) ||
            $repository->existsInStage($matchRequest->StageID, $matchRequest->GuestTeamID)
        ) {
            return response()->json([
                'message' => 'Эта команда уже играет на этом этапе.',
                'errors' => ['Эта команда уже играет на этом этапе.']
            ], 422);
        }
        return Game::create($matchRequest->validated());
    }

    public function players(Game $match)
    {
        return $match->players();
    }


    public function data(Game $match)
    {
        return [
            'players' => $match->players(),
            'goals' => $match->goals()->get(),
            'cards' => $match->cards()->get()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Game $match
     */
    public function update(Request $request, Game $match)
    {
        if ($match->stage->season->is_completed) {
            return response()->json(['error' => 'season is completed'], 403);
        }
        $goals = $request->input('goals');
        $cards = $request->input('cards');

        $match->goals()->delete();
        $match->cards()->delete();

        $teamGoals = [
            $match->HomeTeamID => 0,
            $match->GuestTeamID => 0,
        ];
        foreach ($goals as $goal) {
            //не автогол
            if ($goal['Type'] > -1)
                ++$teamGoals[$goal['TeamID']];
            elseif ($goal['TeamID'] === $match->HomeTeamID)
                ++$teamGoals[$match->GuestTeamID];
            else ++$teamGoals[$match->HomeTeamID];

            Goal::create(array_merge([
                'MatchID' => $match->id,
                'Time' => 0
            ], $goal));
        }

        foreach ($cards as $card) {
            Card::create(array_merge([
                'MatchID' => $match->id,
                'Time' => 0
            ], $card));
        }

        $match->HomeTeamGoals = $teamGoals[$match->HomeTeamID];
        $match->GuestTeamGoals = $teamGoals[$match->GuestTeamID];

        if ($request->input('is_completed'))
            $match->IsCompleted = true;

        $match->save();

        if (in_array($match->Date->format('Y-m-d'), [date('Y-m-d'), date('Y-m-d', strtotime('-1 day'))]))
            MatchMessageService::sendMessage($match);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $match)
    {
        $match->cards()->delete();
        $match->goals()->delete();
        $match->delete();
        return response()->json(['status' => 'OK']);
    }
}
