<?php

namespace App\Http\Resources;

use App\Models\Season;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property-read Season $resource
 */
class SeasonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'league_id' => $this->resource->LeagueID,
            'name' => $this->resource->Name,
            'slug' => $this->resource->slug,
            'league' => $this->whenLoaded('league', [
                'id' => $this->resource->league->id,
                'name' => $this->resource->league->Name,
                'slug' => $this->resource->league->slug,
            ]),
        ];
    }
}
