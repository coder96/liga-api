<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Ramsey\Collection\Collection;

class LeagueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->Name,
            'slug' => $this->resource->slug,
            'seasons' => $this->whenLoaded('seasons',
                $this->resource->seasons->map(function ($season) {
                    return [
                        'id' => $season->id,
                        'league_id' => $season->LeagueID,
                        'name' => $season->Name,
                        'slug' => $season->slug,
                    ];
                })
            )
        ];
    }
}
