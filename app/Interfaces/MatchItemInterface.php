<?php

namespace App\Interfaces;

use App\Models\Player;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Interface PlayerInterface
 * @package App\Interfaces
 *
 * @property int $id
 * @property int $MatchID
 * @property int $TeamID
 * @property int $PlayerID
 * @property int $Type 0 - жк, 1-красный
 * @property int $Time Время внутри игры (0-50 или 0-90)
 * @property Player $player
 */

interface MatchItemInterface {
    public function player() : BelongsTo;
}
