<?php

namespace App\Models;

use App\Enums\StageType;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Team
 *
 * @property int $id
 * @property string $Name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Team newModelQuery()
 * @method static Builder|Team newQuery()
 * @method static Builder|Team query()
 * @method static Builder|Team whereCreatedAt($value)
 * @method static Builder|Team whereId($value)
 * @method static Builder|Team whereName($value)
 * @method static Builder|Team whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $logo
 * @property-read \App\Models\Game|null $nextMatch
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Player[] $players
 * @property-read int|null $players_count
 * @method static Builder|Team whereLogo($value)
 */
class Team extends Model
{
    use HasFactory;

    protected $table = 'team';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name'
    ];

    public function players($seasonId = null): BelongsToMany
    {
        $relation = $this->belongsToMany(
            Player::class,
            PlayerMember::class,
            'TeamID',
            'PlayerId',
            'id',
            'id'
        )
            ->orderBy('FirstName');
        return $seasonId ? $relation->where('SeasonID', '=', $seasonId)
            : $relation;
    }

    public function wins($seasonID)
    {
        $teamID = $this->id;
        return Stage::where('SeasonID', '=', $seasonID)
            ->join('match', 'stage.id', '=','match.StageID', 'inner')
            ->where( function($query) use ($teamID){
                $query->where('match.HomeTeamID', '=', $teamID);
                $query->whereRaw('match.HomeTeamGoals > match.GuestTeamGoals');
                $query->orWhere('match.GuestTeamID', '=', $teamID);
                $query->whereRaw('match.GuestTeamGoals > match.HomeTeamGoals');
            })
            ->count();
    }

    public function draws($seasonID)
    {
        $teamID = $this->id;
        return Stage::where('SeasonID', '=', $seasonID)
            ->join('match', 'stage.id', '=','match.StageID')
            ->where( function($query) use ($teamID) {
                $query->where('match.HomeTeamID', '=', $teamID);
                $query->orWhere('match.GuestTeamID', '=', $teamID);
            })
            ->whereRaw('match.HomeTeamGoals = match.GuestTeamGoals')
            ->count();
    }

    public function losses($seasonID)
    {
        $teamID = $this->id;
        return Stage::where('SeasonID', '=', $seasonID)
            ->join('match', 'stage.id', '=','match.StageID', 'inner')
            ->where( function($query) use ($teamID){
                $query->where('match.HomeTeamID', '=', $teamID);
                $query->whereRaw('match.HomeTeamGoals < match.GuestTeamGoals');
                $query->orWhere('match.GuestTeamID', '=', $teamID);
                $query->whereRaw('match.GuestTeamGoals < match.HomeTeamGoals');
            })
            ->count();
    }

    public function nextMatch()
    {
        return $this->hasOne(Game::class, 'GuestTeamID')
            ->union($this->hasOne(Game::class, 'HomeTeamID'))
            ->having('IsCompleted', '=', 0)
            ->orderBy('id')
            ->take(1);
    }

    public function stats($seasonID)
    {
        $teamID = $this->id;
        $statColumns = [
            'SUM(win) wins',
            'SUM(draw) draws',
            'SUM(loss) losses',
            'SUM(scored) scored',
            'SUM(conceded) conceded',
        ];

        return DB::table( function (\Illuminate\Database\Query\Builder $q) use ($seasonID, $teamID) {
            $columns = [
                '@homeTeam := HomeTeamID = ?',
                '@goals := cast(HomeTeamGoals as signed) - cast(GuestTeamGoals as signed)',
                '@win := if(@homeTeam AND @goals > 0 ' .
                'OR not @homeTeam AND @goals < 0, 1, 0) win',
                '@draw := if(@goals = 0, 1, 0)    draw',
                'if(@win = 0 and @draw = 0, 1, 0) loss',
                'if(@homeTeam, HomeTeamGoals, GuestTeamGoals) scored',
                'if(@homeTeam, GuestTeamGoals, HomeTeamGoals) conceded',
            ];
            $q->selectRaw(join(',', $columns), [$teamID])
                ->from('stage')
                ->join('match', 'stage.id', '=','StageID')
                ->where('SeasonID', '=', $seasonID)
                ->whereIn('type', [StageType::Round, StageType::Group])
                ->whereRaw('IsCompleted = 1')
                ->where( function (\Illuminate\Database\Query\Builder $q) use ($teamID) {
                    $q->where('HomeTeamID','=', $teamID)
                        ->orWhere('GuestTeamID','=', $teamID);
                });
        }, 't')->selectRaw(join(',', $statColumns))
            ->first();
    }
}
