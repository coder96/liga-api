<?php

namespace App\Models;

use App\Interfaces\MatchItemInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Card
 *
 * @property int $id
 * @property int $MatchID
 * @property int $TeamID
 * @property int $PlayerID
 * @property int $Type 0 - жк, 1-красный
 * @property int $Time Время внутри игры (0-50 или 0-90)
 * @method static Builder|Card newModelQuery()
 * @method static Builder|Card newQuery()
 * @method static Builder|Card query()
 * @method static Builder|Card whereId($value)
 * @method static Builder|Card whereMatchID($value)
 * @method static Builder|Card wherePlayerID($value)
 * @method static Builder|Card whereTeamID($value)
 * @method static Builder|Card whereTime($value)
 * @method static Builder|Card whereType($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Player $player
 */
class Card extends Model implements MatchItemInterface
{
    use HasFactory;

    protected $table = 'card';
    public $timestamps = false;

    protected $fillable = [
        'MatchID',
        'TeamID',
        'PlayerID',
        'Type',
        'Time'
    ];

    public static $rules = [
        'MatchID' => 'exist:match',
        'PlayerID' => 'exist:player',
        'TeamID' => 'exist:match',
        'Type' => 'required|number'
    ];

    public function player(): BelongsTo
    {
        return $this->belongsTo(Player::class, 'PlayerID');
    }

    /**
     * @param int $seasonID
     * @param int $type 0 - yellow, 1 - red
     */
    public static function playerMisses(int $seasonID, int $type = 0, int $yellowCardCount = 3)
    {
        $query = self::query()
            ->select([
                'player.id',
                'player.TeamID',
                'team.Name as team'
            ])
            ->selectRaw('(SELECT st2.Name
                    FROM `stage` st2
                    WHERE st2.id > (SELECT MAX(stg.id)
                                    FROM stage stg
                                            INNER JOIN `match` m on stg.id = m.StageID
                                            INNER JOIN card c on m.id = c.MatchID
                        WHERE c.PlayerID = player.id AND c.Type = ?) and st2.SeasonID = stage.SeasonID
                    ORDER BY st2.`id`
                    LIMIT 1
                   ) as `misses`', [$type])
            ->selectRaw('CONCAT(player.FirstName, \' \',player.LastName) AS FullName')
            ->selectRaw('COUNT(card.id) ct, MAX(stage.id) stage, GROUP_CONCAT(stage.Name ORDER BY stage.id SEPARATOR \', \') stages')
            ->join('match', 'card.MatchID', '=', 'match.id')
            ->join('stage', 'match.StageID', '=', 'stage.id')
            ->join('player', 'card.PlayerID', '=', 'player.id')
            ->join('player_member', 'card.PlayerID', '=', 'player_member.PlayerID')
            ->join('team', 'player_member.TeamID', '=', 'team.id')
            ->where('stage.SeasonID', '=', $seasonID)
            ->where('player_member.SeasonID', '=', $seasonID)
            ->where('card.Type', '=', $type)
            ->groupBy('player.id', 'team.id')
            ->orderBy('stage', 'desc');

        if ($type === 0) {
            $query->havingRaw('ct % ' . $yellowCardCount . ' = 0');
        }

        return $query->get();
    }
}
