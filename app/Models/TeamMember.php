<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\TeamMember
 *
 * @property int $id
 * @property int $TeamID
 * @property int $SeasonID
 * @property int|null $Left
 * @property int|null $LastStageID
 * @method static Builder|TeamMember newModelQuery()
 * @method static Builder|TeamMember newQuery()
 * @method static Builder|TeamMember query()
 * @method static Builder|TeamMember whereId($value)
 * @method static Builder|TeamMember whereLastStageID($value)
 * @method static Builder|TeamMember whereLeft($value)
 * @method static Builder|TeamMember whereSeasonID($value)
 * @method static Builder|TeamMember whereTeamID($value)
 * @mixin \Eloquent
 */
class TeamMember extends Pivot
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'team_member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'TeamID',
        'SeasonID',
        'Left',
        'LastStageID',
    ];
}
