<?php

namespace App\Models;

use App\Enums\StageType;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Stage
 *
 * @property int $id
 * @property int $SeasonID
 * @property string $Name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Game[] $matches
 * @property-read int|null $matches_count
 * @method static Builder|Stage newModelQuery()
 * @method static Builder|Stage newQuery()
 * @method static Builder|Stage query()
 * @method static Builder|Stage whereCreatedAt($value)
 * @method static Builder|Stage whereId($value)
 * @method static Builder|Stage whereName($value)
 * @method static Builder|Stage whereSeasonID($value)
 * @method static Builder|Stage whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \App\Models\Season $season
 */
class Stage extends Model
{
    use HasFactory;

    protected $table = 'stage';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'SeasonID',
        'Name',
        'type',
    ];

    protected $casts = [
        'type' => StageType::class,
    ];

    public function matches(): HasMany
    {
        return $this->hasMany(Game::class, 'StageID')->orderBy('Date')->with('guestTeam', 'homeTeam');
    }

    public function season(): BelongsTo
    {
        return $this->belongsTo(Season::class, 'SeasonID');
    }

}
