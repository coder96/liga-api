<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Match
 *
 * @property int $id
 * @property int $StageID
 * @property int $HomeTeamID
 * @property int $GuestTeamID
 * @property int|null $RefereeID
 * @property int $IsCompleted
 * @property mixed $Date
 * @property int $HomeTeamGoals
 * @property int $GuestTeamGoals
 * @property int $MessageID
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Card[] $cards
 * @property-read int|null $cards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Goal[] $goals
 * @property-read int|null $goals_count
 * @property-read \App\Models\Team $guestTeam
 * @property-read \App\Models\Team $homeTeam
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Player[] $players
 * @property-read int|null $players_count
 * @method static Builder|Game newModelQuery()
 * @method static Builder|Game newQuery()
 * @method static Builder|Game query()
 * @method static Builder|Game whereDate($value)
 * @method static Builder|Game whereGuestTeamGoals($value)
 * @method static Builder|Game whereGuestTeamID($value)
 * @method static Builder|Game whereHomeTeamGoals($value)
 * @method static Builder|Game whereHomeTeamID($value)
 * @method static Builder|Game whereId($value)
 * @method static Builder|Game whereIsCompleted($value)
 * @method static Builder|Game whereMessageID($value)
 * @method static Builder|Game whereRefereeID($value)
 * @method static Builder|Game whereStageID($value)
 * @mixin \Eloquent
 * @property-read Stage $stage
 * @property-read Season $season
 * @property-read League $league
 */
class Game extends Model
{
    use HasFactory;

    protected $table = 'match';

    public $timestamps = false;

    public static $rules = [
        'StageID' => ['required', 'exists:stage,id'],
        'HomeTeamID' => ['required', 'exists:team,id'],
        'GuestTeamID' => ['required', 'exists:team,id'],
        'RefereeID' => ['nullable', 'exists:referee,id'],
        'Date' => ['required']
    ];

    protected $fillable = [
        'StageID',
        'HomeTeamID',
        'GuestTeamID',
        'RefereeID',
        'IsCompleted',
        'Date'
    ];

    protected $casts = [
        'Date' => 'date:Y-m-d H:i',
    ];

    public function homeTeam(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'HomeTeamID');
    }

    public function guestTeam(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'GuestTeamID');
    }

    public function players()
    {
        $seasonId = $this->stage->SeasonID;

        $homeTeamPlayers = $this->homeTeam->players($seasonId)->get();
        foreach ($homeTeamPlayers as $teamPlayer) {
            $teamPlayer->TeamID = $this->HomeTeamID;
        }

        $guestTeamPlayers = $this->guestTeam->players($seasonId)->get();
        foreach ($guestTeamPlayers as $teamPlayer) {
            $teamPlayer->TeamID = $this->GuestTeamID;
        }

        return $homeTeamPlayers->merge($guestTeamPlayers);
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'MatchID');
    }

    public function goals()
    {
        return $this->hasMany(Goal::class, 'MatchID');
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class, 'StageID');
    }

    public function lastMatches()
    {
        return self::select()
            ->where('IsCompleted', '=', 1)
            ->orderBy('Date', 'DESC')
            ->take(2)
            ->with(['homeTeam', 'guestTeam', 'stage.season.league'])
            ->get();
    }
}
