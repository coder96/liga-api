<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Player
 *
 * @property int $id
 * @property int|null $TeamID
 * @property string $LastName
 * @property string $FirstName
 * @property string $BirthYear
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Player newModelQuery()
 * @method static Builder|Player newQuery()
 * @method static Builder|Player query()
 * @method static Builder|Player whereBirthYear($value)
 * @method static Builder|Player whereCreatedAt($value)
 * @method static Builder|Player whereFirstName($value)
 * @method static Builder|Player whereId($value)
 * @method static Builder|Player whereLastName($value)
 * @method static Builder|Player whereTeamID($value)
 * @method static Builder|Player whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Team|null $team
 */
class Player extends Model
{
    use HasFactory;

    protected $table = 'player';

    protected $fillable = ['TeamID', 'LastName', 'FirstName', 'BirthYear'];

    public static $rules = [
        'TeamID' => 'required|exists:team,id',
        'LastName' => 'required|string',
        'FirstName' => 'required|string',
        'BirthYear' => 'nullable|integer',
        'SeasonID' => 'nullable|integer',
    ];


    public function getFullName(): string
    {
        return "$this->FirstName $this->LastName";
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'TeamID');
    }

    public function leftFromTeam(int $teamId, int $seasonId): int
    {
        return DB::table('player_member')
            ->where('PlayerID', '=', $this->id)
            ->where('TeamID', '=', $teamId)
            ->where('SeasonID', '=', $seasonId)
            ->update([
                'Left' => 1
            ]);
    }
}
