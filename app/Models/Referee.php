<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Referee
 *
 * @property int $id
 * @property string $Name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Referee newModelQuery()
 * @method static Builder|Referee newQuery()
 * @method static Builder|Referee query()
 * @method static Builder|Referee whereCreatedAt($value)
 * @method static Builder|Referee whereId($value)
 * @method static Builder|Referee whereName($value)
 * @method static Builder|Referee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Referee extends Model
{
    use HasFactory;

    protected $table = 'referee';
}
