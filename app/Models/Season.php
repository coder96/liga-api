<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * App\Models\Season
 *
 * @property int $id
 * @property int $yellow_card_count
 * @property int $LeagueID
 * @property string $Name
 * @property int $is_completed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stage[] $stages
 * @property-read int|null $stages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Team[] $teams
 * @property-read int|null $teams_count
 * @method static Builder|Season newModelQuery()
 * @method static Builder|Season newQuery()
 * @method static Builder|Season query()
 * @method static Builder|Season whereCreatedAt($value)
 * @method static Builder|Season whereId($value)
 * @method static Builder|Season whereLeagueID($value)
 * @method static Builder|Season whereName($value)
 * @method static Builder|Season whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\League $league
 */
class Season extends Model
{
    use HasFactory;

    protected $table = 'season';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'LeagueID',
        'Name'
    ];

    public function teams(): HasManyThrough
    {
        return $this->hasManyThrough(
            Team::class,
            TeamMember::class,
            'SeasonID',
            'id',
            'id',
            'TeamId'
        );
    }

    public function stages(): HasMany
    {
        return $this->hasMany(Stage::class, 'SeasonID')->with('matches');
    }

    public function table($seasonID)
    {
        $teams = $this->teams()->get();
        $fields = [
            'wins',
            'draws',
            'losses',
            'scored',
            'conceded',
        ];

        /**
         * @var $teams Team[]
         */
        foreach ($teams as $team) {
            $stat = $team->stats($seasonID);
            foreach ($fields as $field) {
                $team->$field = $stat->$field ?? 0;
            }

            $team->points = $team->wins * 3 + $team->draws;
            $team->plays = $team->wins + $team->draws + $team->losses;
            $team->difference = $team->scored - $team->conceded;
        }

        $result = $teams->toArray();
        usort($result, function ($a, $b) {
            $points = $b['points'] <=> $a['points'];
            $difference = $b['difference'] <=> $a['difference'];
            $scored = $b['scored'] <=> $a['scored'];
            $conceded = $a['conceded'] <=> $b['conceded'];

            return $points ? $points :
                ($difference ? $difference :
                    ($scored ? $scored :
                        ($conceded ? $conceded : 0)));
        });

        return $result;
    }

    public function bombardiers()
    {
        return DB::table('goal')
            ->selectRaw('player.id, CONCAT(player.FirstName, \' \', player.LastName) FullName, COUNT(goal.id) goals, team.Name team, team.id team_id')
            ->join('player', 'goal.PlayerID', '=', 'player.id')
            ->join('player_member', 'player_member.PlayerID', '=', 'player.id')
            ->join('team', 'player_member.TeamID', '=', 'team.id')
            ->join('match', 'MatchID', '=', 'match.id')
            ->join('stage', 'StageID', '=', 'stage.id')
            ->where('stage.SeasonID', '=', $this->id)
            ->where('player_member.SeasonID', '=', $this->id)
            ->where('goal.Type','<>','-1')
            ->groupBy('player.id')
            ->orderBy('goals','desc')
            ->take(10)
            ->get();
    }

    public function violators ()
    {
        return DB::table('card')
            ->selectRaw('`player`.`id`, CONCAT(`player`.`FirstName`, \' \', `player`.`LastName`) FullName,' .
                'COUNT(`card`.`id`) `cards`, ' .
                '`team`.`Name` team, ' .
                '`team`.`id` team_id, ' .
                'SUM(IF (`card`.`Type` = 0, 1, 0)) yellow,' .
                'SUM(IF (`card`.`Type` = 1, 1, 0)) red')
            ->join('player', 'card.PlayerID', '=', 'player.id')
            ->join('player_member', 'player_member.PlayerID', '=', 'player.id')
            ->join('team', 'player_member.TeamID', '=', 'team.id')
            ->join('match', 'MatchID', '=', 'match.id')
            ->join('stage', 'match.StageID', '=', 'stage.id')
            ->where('stage.SeasonID', '=', $this->id)
            ->where('player_member.SeasonID', '=', $this->id)
            ->groupBy('player.id')
            ->orderBy('cards','desc')
            ->take(5)
            ->get();
    }

    public function league(): BelongsTo
    {
        return $this->belongsTo(League::class, 'LeagueID');
    }

}
