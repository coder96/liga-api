<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\PlayerMember
 *
 * @property int $id
 * @property int $TeamID
 * @property int $SeasonID
 * @property int $Left
 * @property int $LastStageID
 * @method static Builder|PlayerMember newModelQuery()
 * @method static Builder|PlayerMember newQuery()
 * @method static Builder|PlayerMember query()
 * @method static Builder|PlayerMember whereId($value)
 * @method static Builder|PlayerMember whereLastStageID($value)
 * @method static Builder|PlayerMember whereLeft($value)
 * @method static Builder|PlayerMember whereSeasonID($value)
 * @method static Builder|PlayerMember whereTeamID($value)
 * @mixin \Eloquent
 */
class PlayerMember extends Pivot
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'PlayerID',
        'TeamID',
        'SeasonID'
    ];
}
