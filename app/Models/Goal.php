<?php

namespace App\Models;

use App\Interfaces\MatchItemInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Goal
 *
 * @property int $id
 * @property int $MatchID
 * @property int $TeamID
 * @property int|null $PlayerID
 * @property int|null $AssistantID
 * @property int|null $Time Время внутри игры (0-50 или 0-90)
 * @property int $Type -1 автогол, 0 - гол в игре, 1-со штрафного, 2 с пенальти
 * @method static Builder|Goal newModelQuery()
 * @method static Builder|Goal newQuery()
 * @method static Builder|Goal query()
 * @method static Builder|Goal whereAssistantID($value)
 * @method static Builder|Goal whereId($value)
 * @method static Builder|Goal whereMatchID($value)
 * @method static Builder|Goal wherePlayerID($value)
 * @method static Builder|Goal whereTeamID($value)
 * @method static Builder|Goal whereTime($value)
 * @method static Builder|Goal whereType($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Player $player
 */
class Goal extends Model implements MatchItemInterface
{
    use HasFactory;

    protected $table = 'goal';

    public $timestamps = false;

    protected $fillable = [
        'MatchID',
        'TeamID',
        'PlayerID',
        'Type',
        'Time'
    ];

    public static $rules = [
        'MatchID' => 'exist:match',
        'PlayerID' => 'exist:player',
        'TeamID' => 'exist:match',
        'Type' => 'required|number'
    ];

    public function player(): BelongsTo
    {
        return $this->belongsTo(Player::class, 'PlayerID');
    }
}
