<?php

namespace App\Console\Commands;

use App\Services\MatchMessageService;
use Illuminate\Console\Command;

class NotifyTodayMatchesCommand extends Command
{
    protected $signature = 'notify-today-matches';

    protected $description = 'Command description';

    public function handle()
    {
        MatchMessageService::notify();
    }
}
