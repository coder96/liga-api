<?php


namespace App\Services;


define('CHAT_ID', env('TELEGRAM_CHAT_ID', 'myTestChannelDon'));

class TelegramBot
{
    private const CHAT_ID = '@' . CHAT_ID;
    private const BOT_URL = 'https://api.telegram.org/bot';

    /**
     * @param $message
     * @return false|int
     */
    public static function sendMessage($message)
    {
        $botToken = env('TELEGRAM_BOT_TOKEN');
        $bot_url    = self::BOT_URL . "$botToken/";

        $url = $bot_url . "sendMessage?chat_id=" . self::CHAT_ID . "&text=".urlencode($message);

        $res = json_decode(file_get_contents($url), true);

        return $res['ok'] ? $res['result']['message_id'] : false;
    }


    /**
     * @param $message
     * @param $messageID
     * @return bool
     */
    public static function editMessage($message, $messageID): bool
    {
        $botToken = env('TELEGRAM_BOT_TOKEN');
        $bot_url    = self::BOT_URL . "$botToken/";

        $url = $bot_url. 'editMessageText?chat_id=' . self::CHAT_ID . "&message_id=$messageID&text=".urlencode($message);

        return file_get_contents($url);
    }
}
