<?php

namespace App\Services;

use App\Http\Requests\StagePostRequest;
use App\Repositories\StageRepository;

class StageService
{
    public function __construct(
        private readonly StageRepository $repository
    )
    {
    }

    public function create(array $data)
    {
        return $this->repository->create($data);
    }
}
