<?php

namespace App\Services;


use App\Interfaces\MatchItemInterface;
use App\Models\Game;
use Illuminate\Database\Eloquent\Collection;

class MatchMessageService
{
    private const EOL = PHP_EOL . PHP_EOL;

    public const RED_CARD = '🟥';
    public const YELLOW_CARD = '🟨';

    public const GOAL_TYPES = [
        '-1' => '(Автогол)',
        '0' => '⚽',
        '1' => '⚽',
        '2' => '⚽(Пенальти)'
    ];

    public const CARD_TYPES = [
        '0' => self::YELLOW_CARD,
        '1' => self::RED_CARD
    ];

    public static function sendMessage(Game $match)
    {
        $league = $match->stage->season->league->Name;
        $homeTeam = $match->homeTeam()->first()->Name;
        $homeTeamGoals = $match->HomeTeamGoals;

        $guestTeam = $match->guestTeam()->first()->Name;
        $guestTeamGoals = $match->GuestTeamGoals;

        $message = $league . self::EOL . $match->stage()->first()->Name . self::EOL
            . "$homeTeam $homeTeamGoals : $guestTeamGoals $guestTeam"
            . self::EOL;

        $cards = $match->cards()->with('player')->get();
        $message .= self::processMatchItems($match, $cards, 'cards', $homeTeam, $guestTeam);

        if (!$match->IsCompleted) {
            $goals = $match->goals()->with('player')->get();
            $message .= self::processMatchItems($match, $goals, 'goals', $homeTeam, $guestTeam);

            $message .= 'Игра продолжается!';
        } else $message .= 'https://liga.kg/matches/' . $match->id;

        if ($match->MessageID)
            TelegramBot::editMessage($message, $match->MessageID);
        elseif ($messageID = TelegramBot::sendMessage($message)) {
            $match->MessageID = $messageID;

            $match->save();
        }

    }

    /**
     * @param Game $match
     * @param Collection|MatchItemInterface[] $items
     * @param string $type goals|card
     * @param string $homeTeam
     * @param string $guestTeam
     * @return string
     */
    private static function processMatchItems(Game $match, Collection $items, string $type, string $homeTeam, string $guestTeam): string
    {
        $res = [
            'home' => '',
            'guest' => ''
        ];

        $types = $type === 'goals' ? self::GOAL_TYPES : self::CARD_TYPES;

        foreach ($items as $item) {
            $playerFullName = $item->player->getFullName();
            $itemType = $types["$item->Type"] ?? '';

            if ($item->TeamID === $match->HomeTeamID)
                $res['home'] .= "$itemType $playerFullName" . PHP_EOL;
            else
                $res['guest'] .= "$itemType $playerFullName" . PHP_EOL;
        }

        $text = '';
        if (!empty($res['home']) || !empty($res['guest'])) {
            $text .= ($type === 'goals' ? '#голы' : '#карточки') . PHP_EOL;
        }

        if (!empty($res['home']))
            $text .= $homeTeam . ':' . PHP_EOL . $res['home'] . PHP_EOL;

        if (!empty($res['guest']))
            $text .= $guestTeam . ':' . PHP_EOL . $res['guest'] . self::EOL;

        return $text;
    }

    public static function notify(): void
    {
        $matches = Game::query()
            ->where('IsCompleted', '=', 0)
            ->where('Date', '>=', date('Y-m-d'))
            ->where('Date', '<', date('Y-m-d', strtotime('+1 day')))
            ->orderBy('StageID', 'ASC')
            ->orderBy('Date', 'ASC')
            ->with(['homeTeam', 'guestTeam', 'stage'])
            ->get();

        if ($matches->count() === 0) {
            return;
        }

        $message = 'Сегодняшние матчи';
        $prevStage = 0;

        foreach ($matches as $match) {
            $league = $match->stage->season->league->Name;
            $stage = $prevStage !== $match->StageID ?
                self::EOL . $league
                . self::EOL . $match->stage->Name . self::EOL
                : '';
            $homeTeam = $match->homeTeam->Name;

            $guestTeam = $match->guestTeam->Name;

            $message .= $stage
                . $match->Date->format('H:i') . " $homeTeam vs $guestTeam"
                . PHP_EOL;
            $prevStage = $match->StageID;
        }

        if ($matches->count()) {
            $message .= self::EOL . 'https://liga.kg';
            TelegramBot::sendMessage($message);
        }
    }
}
