<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Attributes\Description;
use BenSampo\Enum\Enum;

/**
 * @method static static Round()
 * @method static static Group()
 * @method static static PlayOff()
 */
final class StageType extends Enum
{
    #[Description('Круговой')]
    const Round = 0;

    #[Description('Групповой этап')]
    const Group = 1;

    #[Description('Плей-офф')]
    const PlayOff = 2;
}
