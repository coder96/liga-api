<?php

namespace App\Repositories;

use App\Models\Game;
use App\Models\Stage;
use Illuminate\Database\Eloquent\Builder;

class MatchRepository
{
    public function existsInStage(int $stageId, int $teamId): bool
    {
        return Game::query()
            ->where('StageID', $stageId)
            ->where(function (Builder $builder) use ($teamId) {
                $builder->where('HomeTeamID', $teamId)
                    ->orWhere('GuestTeamID', $teamId);
            })
            ->exists();
    }
}
