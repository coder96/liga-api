<?php

namespace App\Repositories;

use App\Models\Stage;

class StageRepository
{
    public function create(array $data)
    {
        return Stage::create($data);
    }
}
