<?php

namespace Database\Seeders;

use App\Models\Player;
use Illuminate\Database\Seeder;

class PlayerMember extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $players = Player::all();
        foreach ($players as $player) {
            \App\Models\PlayerMember::create([
                'PlayerID' => $player->id,
                'TeamID' => $player->TeamID,
                'SeasonID' => 1,
            ]);
        }
    }
}
