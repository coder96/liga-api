<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goal', function (Blueprint $table) {
            $table->foreignId('AssistantID')->nullable()->change();

            $table->boolean('GoalType')
                ->comment('-1 автогол, 0 - гол в игре, 1-со штрафного, 2 с пенальти')
                ->change();

            $table->boolean('Time')
                ->nullable()
                ->change();

            $table->renameColumn('GoalType', 'Type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goal', function (Blueprint $table) {
            $table->foreignId('AssistantID')->nullable(false)->change();
            $table->tinyInteger('Time')->nullable(false)->change();

            $table->renameColumn('Type', 'GoalType');
        });
    }
}
