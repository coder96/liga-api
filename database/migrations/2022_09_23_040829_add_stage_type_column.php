<?php

use App\Enums\StageType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stage', function (Blueprint $table) {
            $table->tinyInteger(column: 'type', unsigned: true)
                ->default(StageType::Round);

            $table->unique([
                'SeasonID',
                'Name',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stage', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
};
