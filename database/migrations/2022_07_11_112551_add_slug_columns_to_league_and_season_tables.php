<?php

use App\Models\League;
use App\Models\Season;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugColumnsToLeagueAndSeasonTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('league', function (Blueprint $table) {
            $table->string('slug')
                ->unique()
                ->nullable();
        });

        Schema::table('season', function (Blueprint $table) {
            $table->string('slug')
                ->nullable();

            $table->unique([
                'LeagueID',
                'slug',
            ]);
        });

        $transliterator = new \ElForastero\Transliterate\Transliterator(\ElForastero\Transliterate\Map::LANG_RU);

        $seasons = Season::all();
        foreach ($seasons as $season) {
            $season->slug = $transliterator->slugify($season->Name);
            $season->save();
        }

        $leagues = League::all();
        foreach ($leagues as $league) {
            $league->slug = $transliterator->slugify($league->Name);
            $league->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('league', function (Blueprint $table) {
            $table->dropColumn('slug');
        });

        Schema::table('season', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
