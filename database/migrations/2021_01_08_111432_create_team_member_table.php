<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_member', function (Blueprint $table) {
            $table->id();
            $table->foreignId('TeamID')
                ->constrained('team')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('SeasonID')
                ->constrained('season')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->boolean('Left');
            $table->foreignId('LastStageID')
                ->constrained('stage')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_member');
    }
}
