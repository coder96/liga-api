<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlayerMemberModify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_member', function (Blueprint $table) {
            $table->foreignId('PlayerID')
                ->constrained('player')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->boolean('Left')
                ->default(0)
                ->change();
            $table->foreignId('LastStageID')
                ->nullable()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_member', function (Blueprint $table) {
            $table->dropConstrainedForeignId('PlayerID');
        });
    }
}
