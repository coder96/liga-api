<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goal', function (Blueprint $table) {
            $table->id();
            $table->foreignId('MatchID')
                ->constrained('match')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('TeamID')
                ->constrained('team')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('PlayerID')
                ->nullable()
                ->constrained('player')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('AssistantID')
                ->constrained('player')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->tinyInteger('GoalType', false, true)
                ->comment('0 - гол в игре, 1-со штрафного, 2 с пенальти');
            $table->tinyInteger('Time', false, true)
                ->comment('Время внутри игры (0-50 или 0-90)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal');
    }
}
