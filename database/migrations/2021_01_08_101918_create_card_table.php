<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card', function (Blueprint $table) {
            $table->id();
            $table->foreignId('MatchID')
                ->constrained('match')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('TeamID')
                ->constrained('team')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('PlayerID')
                ->constrained('player')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->tinyInteger('Type', false, true)
                ->comment('0 - жк, 1-красный');
            $table->tinyInteger('Time', false, true)
                ->comment('Время внутри игры (0-50 или 0-90)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card');
    }
}
