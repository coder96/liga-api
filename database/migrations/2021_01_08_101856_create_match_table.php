<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match', function (Blueprint $table) {
            $table->id();
            $table->foreignId('StageID')
                ->constrained('stage')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('HomeTeamID')
                ->constrained('team')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('GuestTeamID')
                ->constrained('team')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('RefereeID')
                ->nullable()
                ->constrained('referee')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->boolean('IsCompleted')
                ->default(0);
            $table->dateTime('Date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match');
    }
}
